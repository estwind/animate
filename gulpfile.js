'use strict';

let gulp = require('gulp'),
	plumber = require('gulp-plumber'),
	browserSync  = require('browser-sync'),
	reload = browserSync.reload,
	rimRaf = require('rimraf'),
	sass = require('gulp-sass'),
	sourcemaps = require('gulp-sourcemaps'),
	uglify = require('gulp-uglify'),
	cssMin = require('gulp-clean-css'),
	preFixer = require('gulp-autoprefixer');

let path = {

	build: {
		html: 'build/',
		js: 'build/js',
		css: 'build/css/',
		img: 'build/img/',
		fonts: 'build/font/'
	},
	src: {
		html: 'src/*.html',
		mainjs: 'src/*.js',
		js: 'src/js/',
		style: 'src/*.sass',
		css: 'src/css/',
		img: 'src/img/*',
		fonts: 'src/font/*'
	},
	watch: {
		html: 'src/*.html',
		js: 'src/**/*.js',
		style: 'src/*.sass',
		img: 'src/img/*'
	},
	clean: './build'
};

let config = {
	server: {
		baseDir: "./build"
	},
	tunnel: false,
	host: 'localhost',
	port: 3000,
	logPrefix: "frontend"
};

gulp.task('html:build', function () {
	return gulp.src(path.src.html)
		.pipe(gulp.dest(path.build.html))
		.pipe(reload({stream: true}));
});

gulp.task('img:build', function() {
	return gulp.src(path.src.img)
		.pipe(gulp.dest(path.build.img));
});

gulp.task('style:build', function () {
	gulp.src(path.src.style)
		.pipe(sourcemaps.init())
		.pipe(sass())
		.pipe(preFixer({
			cascade: false
		}))
        .pipe(cssMin())
        .pipe(sourcemaps.write('.'))
		.pipe(gulp.dest(path.build.css))
        .pipe(gulp.dest(path.src.css))
		.pipe(reload({stream: true}));

	return gulp.src(path.src.fonts)
		.pipe(gulp.dest(path.build.fonts))
});

gulp.task('js:build', function () {
	return gulp.src(path.src.mainjs)
		.pipe(plumber())
		.pipe(uglify())
		.pipe(gulp.dest(path.src.js))
		.pipe(gulp.dest(path.build.js))
		.pipe(reload({stream: true}));
});

gulp.task('fonts:build', function () {
	return gulp.src(path.src.fonts)
		.pipe(gulp.dest(path.build.fonts))
		.pipe(reload({stream: true}));
});

gulp.task('build', gulp.parallel('html:build', 'style:build', 'js:build', 'img:build', 'fonts:build'));

gulp.task('clear', function (callback) {
	return rimRaf(path.clean, callback);
});

gulp.task('browser-sync', function () {
	browserSync(config);
});

gulp.task('watch', function(){
	gulp.watch(path.watch.html, gulp.parallel('html:build'));
	gulp.watch(path.watch.style, gulp.parallel('style:build'));
	gulp.watch(path.watch.js, gulp.parallel('js:build'));
	gulp.watch(path.watch.img, gulp.parallel('img:build'));
});

gulp.task('default', gulp.parallel('build', 'browser-sync', 'watch'));




