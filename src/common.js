
document.addEventListener('DOMContentLoaded', function(){

    function animation(){
        document.querySelector('.tabs-nav__link').classList.add('active');
        document.querySelector('.bg').classList.add('animate');
    }
    document.querySelector('.click').addEventListener('click', animation);

    function collapse(){
        document.querySelector('.bg').classList.toggle('active_tab');
    }
    document.querySelector('.collapse').addEventListener('click', collapse);
});
